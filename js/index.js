$(function(){
					$("[data-toggle='tooltip']").tooltip();
					$("[data-toggle='popover']").popover();
					$('.carousel').carousel({
						interval: 5000
					});

					$('#exampleModal').on('show.bs.modal', function (e){
						console.log('El modal se esta mostrando');

						$('#mostrar').removeClass('btn-outline-success');
						$('#mostrar').addClass('btn-primary');
						$('#mostrar').prop('disabled', true);
					});
					$('#exampleModal').on('shown.bs.modal', function (e){
						console.log('El modal se mostro');
					});
					$('#exampleModal').on('hide.bs.modal', function (e){
						console.log('El modal se esta ocultando');
					});
					$('#exampleModal').on('hidden.bs.modal', function (e){
						console.log('El modal se oculto');
						$('#mostrar').prop('disabled', false);
					});

});		